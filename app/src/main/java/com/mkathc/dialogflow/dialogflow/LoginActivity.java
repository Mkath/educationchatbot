package com.mkathc.dialogflow.dialogflow;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class LoginActivity extends AppCompatActivity{
    private TextInputEditText etUser;
    private TextInputEditText etPass;
    private Button btnLogin;
    private LinearLayout mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUser = findViewById(R.id.etUser);
        etPass = findViewById(R.id.etPass);

        mainView = findViewById(R.id.mainView);

        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

    }


    private void loginUser() {

        if(etUser.getText().toString().equals("prueba@gmail.com") && etPass.getText().toString().equals("123456")){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }else{
            Snackbar.make(mainView, "Error al iniciar sesión",2000 ).show();
        }
    }


}
